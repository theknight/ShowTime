﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceProcess;
using System.Diagnostics;

namespace ShowTimeUI
{
    /// <summary>
    /// 控制工具类
    /// </summary>
    public class ContralUtil
    {
        public static void install() {
            string CurrentDirectory = System.Environment.CurrentDirectory;
            System.Environment.CurrentDirectory = CurrentDirectory + "\\Service";
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.FileName = "Install.bat";
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            System.Environment.CurrentDirectory = CurrentDirectory;
        }

        public static void Uninstall() {
            string CurrentDirectory = System.Environment.CurrentDirectory;
            System.Environment.CurrentDirectory = CurrentDirectory + "\\Service";
            Process process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.FileName = "Uninstall.bat";
            process.StartInfo.CreateNoWindow = true;
            process.Start();
            System.Environment.CurrentDirectory = CurrentDirectory;
        }

        public static void start() {
            ServiceController serviceController = new ServiceController("ShowTimeService");
            serviceController.Start();
        }

        public static void stop()
        {
            ServiceController serviceController = new ServiceController("ShowTimeService");
            if (serviceController.CanStop)
                serviceController.Stop();
        }

        public static void pause() {
            ServiceController serviceController = new ServiceController("ShowTimeService");
            if (serviceController.CanPauseAndContinue)
            {
                if (serviceController.Status == ServiceControllerStatus.Running)
                    serviceController.Pause();
                else if (serviceController.Status == ServiceControllerStatus.Paused)
                    serviceController.Continue();
            }
        }

        public static String checkState() {
            ServiceController serviceController = new ServiceController("ShowTimeService");
            string status = serviceController.Status.ToString();
            return status;
        }
    }
}
