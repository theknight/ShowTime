﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShowTimeUI
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private System.Timers.Timer timer = new System.Timers.Timer(4000);
        private delegate void myDelegate();

        public MainWindow()
        {
            InitializeComponent();
            this.Opacity = 0;
            WinPosition();
            getNowTime();
            initTimer();
        }

        /// <summary>
        /// 窗体显示时间
        /// </summary>
        public void getNowTime() {
            String time = DateTime.Now.ToString("HH:mm");
            this.tb_hour.Text = time.Split(':')[0];
            this.tb_min.Text = time.Split(':')[1];
        }

        /// <summary>
        /// 设置窗体在屏幕的位置
        /// </summary>
        public void WinPosition()
        {
            double ScreenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;//WPF
            this.Top = 20;
            this.Left = ScreenWidth - 210;
        }

        private void initTimer() {
            timer.Elapsed += Timer_Elapsed;
            timer.AutoReset = false;
            timer.Enabled = true;
            timer.Start();

            Timer timer_window = new Timer();
            timer_window.Interval = 100;
            timer_window.Tick += Timer_window_Tick;
            timer_window.Enabled = true;
            timer_window.Start();
        }

        private void Timer_window_Tick(object sender, EventArgs e)
        {
            this.Opacity += 0.1;
            if (Opacity == 100)
            {
                ((Timer)sender).Stop();
            }
        }

        /// <summary>
        /// 到时间自动关闭应用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            myDelegate myDele = new myDelegate(closeApp);
            this.Dispatcher.Invoke(new Action(myDele));
        }

        private void closeApp() {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
