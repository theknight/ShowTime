﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace ShowTimeService
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ShowTimeService()
            };
            ServiceBase.Run(ServicesToRun);

            //Process process = new Process();
            //process.StartInfo.FileName = "ShowTimeUI.exe";
            ////process.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\";
            //process.StartInfo.WorkingDirectory = @"E:\DoNet\ShowTimeService\ShowTimeService\bin\Debug\";
            //process.StartInfo.CreateNoWindow = false;
            //process.Start();
        }
    }
}
