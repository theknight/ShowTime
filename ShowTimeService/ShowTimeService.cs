﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ShowTimeService
{
    public partial class ShowTimeService : ServiceBase
    {
        private String hour = "0", min = "0";

        public ShowTimeService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("ShowTimeService启动");//在系统事件查看器里的应用程序事件里来源的描述   
            System.Timers.Timer t = new System.Timers.Timer();
            t.Interval = 3000;
            t.Elapsed += new System.Timers.ElapsedEventHandler(ChkSrv);//到达时间的时候执行事件；    
            t.AutoReset = true;//设置是执行一次（false）还是一直执行(true)；    
            t.Enabled = true;//是否执行System.Timers.Timer.Elapsed事件；
            t.Start();
        }

        private void ChkSrv(object sender, ElapsedEventArgs e)
        {
            int intHour = e.SignalTime.Hour;
            int intMinute = e.SignalTime.Minute;
            int intSecond = e.SignalTime.Second;

            if (intMinute == 0 || intMinute == 30)
            {
                if (!min.Equals(intMinute+""))
                {
                    min = intMinute + "";
                    try
                    {
                        //Process process = new Process();
                        //process.StartInfo.FileName = "ShowTimeUI.exe";
                        //process.StartInfo.WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory + "\\";
                        //process.StartInfo.CreateNoWindow = false;
                        //process.StartInfo.UseShellExecute = false;
                        //process.Start();

                        WinAPI_Interop.createProcess2(AppDomain.CurrentDomain.BaseDirectory + "ShowTimeUI.exe");
                    }
                    catch (Exception ex)
                    {
                        this.Stop();
                    }
                }
            }
        }

        protected override void OnStop()
        {
        }
    }
}
